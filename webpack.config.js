var path = require("path");                     // El módulo path lo tenemos por tener instalado NodeJS
var webpack = require("webpack");

module.exports = {                              // Sintaxis CommonJS, sólo la usaremos aquí
    entry: "./src/index.js",                    // Script de entrada a la aplicación
    output: {
        filename: "bundle.js",                  // Nombre del bundle de salida
        path: path.resolve(__dirname, "dist"),  // Ruta física absoluta donde se guardará el bundle
        publicPath: "/dist/"                    // URL pública para acceder al bundle
    },
    devServer: { 
        contentBase: path.join(__dirname, "public"),
        open: true,
        hot: true
    },
    devtool: "inline-source-map",               // Generará source maps dentro de los mismos archivos JS
    module: {
        rules: [
            {
                test: /\.scss$/,                                    // Para los archivos con extensión .scss
                use: ["style-loader", "css-loader", "sass-loader"]  // utiliza primero sass-loader, luego css-loader y luego style-loader
            },
            {
                test: /\.js$/,                  // Para los archivos con extensión .js
                use: {
                    loader: "babel-loader",     // usa el cargador de Babel
                    query: {
                        presets: ["env"]        // con el preset env
                    }
                },
                exclude: /node_modules/         // excepto en el directorio node_modules
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf|svg)$/,    // Para las tipografías
                use: ["file-loader"]                        // usa el cargador file-loader
            },
            {
                test: /\.html$/,                // Para los archivos con extensión .html
                use: ["raw-loader"]             // usa el cargador raw-loader  (para que webpack-dev-server refresque cuando cambiemos el HTML)
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
}